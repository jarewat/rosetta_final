const express = require('express')
const router = express.Router()
const jobController = require('../controller/JobsController')

/* GET users listing. */
router.get('/', jobController.getJobs)

// router.get('/:id', jobController.getAnm)

router.post('/', jobController.addJob)

// router.put('/', jobController.updateAnm)

// router.delete('/:id', jobController.deleteAnm)

module.exports = router
