const Announcement = require('../models/Announcement')
const Job = require('../models/Job')

const announcementContrller = {
  addAnm(req, res, next) {
    const payload = req.body
    const anm = new Announcement(payload)
    anm
      .save()
      .then(() => {
        res.json(anm)
      })
      .catch(err => {
        res.status(500).send(err)
      })
  },
  async updateAnm(req, res, next) {
    const { id } = req.params
    const payload = req.body
    try {
      const anm = await Announcement.updateOne({ _id: payload._id }, payload)
      res.json(anm)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  deleteAnm(req, res, next) {
    const { id } = req.params
    Announcement.deleteOne({ _id: id })
      .then(() => {
        res.json({ id })
      })
      .catch(err => {
        res.status(500).send(error)
      })
  },
  getAnms(req, res, next) {
    Announcement.find({})
      .populate('job')
      .exec(function (err, anms) {
        if (err) {
          console.log(err)
        }
        res.json({ anms })

      });
  },
  getAnm(req, res, next) {
    const { id } = req.params
    Announcement.findById(id, {})
      .populate('job')
      .exec(function (err, anms) {
        if (err) {
          console.log(err)
        }
        res.json({ anms })
      });
  }
}

module.exports = announcementContrller
