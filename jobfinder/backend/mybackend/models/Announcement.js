const mongoose = require('mongoose')
const Schema = mongoose.Schema

mongoose.connect('mongodb://admin:password@localhost/jobfinder', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('mongodb Announcement connected')
})

const announcementSchema = new Schema({
  name: String,
  job: { type: Schema.Types.ObjectId, ref: 'Job' },
  amount: Number,
  address: String,
  min_salary: Number,
  max_salary: Number,
  description: String,
  benefit: String,
  gender: String,
  min_age: Number,
  max_age: Number,
  educate: String,
  min_experience: Number,
  max_experience: Number,
  new_graduates: Boolean,
  properties: String,
  status: Boolean,
  type_job: String,
  date_open: Date,
  date_close:Date,
  skill:String
})
module.exports = mongoose.model('Announcement', announcementSchema)
