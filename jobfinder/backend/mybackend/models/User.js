const mongoose = require('mongoose')
const Schema = mongoose.Schema

mongoose.connect('mongodb://admin:password@localhost/jobfinder', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('mongodb User connected')
})

const userSchema = new Schema({
  name: String,
  gender: String
})

module.exports = mongoose.model('User', userSchema)
