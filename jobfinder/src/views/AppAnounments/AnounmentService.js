const anounmentService = {
  AnmsList: [
    {
      id: 1,
      name: 'รับงานตำแหน่ง dataSciene',
      career: 'programmer/it',
      position: 'dataSciene',
      type: 'ประจำ',
      companyAddress: 'ตึกกรุงเทพ ชั้น 5 เลขที่ 179 ถนนสาทรใต้ เขตทุ่งมหาเมฆ',
      cityAddress: 'Chonburi',
      salary: 50000,
      amount: 1,
      description: 'เขียนโปรแกรม และ ออกแบบ UI',
      benefit: '-ค่าเดินทาง -ค่าที่พัก -ค่าอาหาร -ค่าโทรศัพท์ -ประกันสังคม',
      gender: 'All gender',
      age: 21,
      educate: 'Ph.D.',
      yearExperience: 2,
      newGraduates: true,
      properties:
        'สัญชาติไทย สามารถ อ่าน เขียน และสนทนาทั่วไปเป็นภาษาอังกฤษได้ มีประสบการณ์ทำงานมากกว่า 5 ปีในการพัฒนาระบบ มีทักษะในการดูแลผู้ใต้บังคับบัญชาจะพิจารณาเป็นพิเศษ มีประสบการณ์ในงานด้านผู้ขายเพื่อสนับสนุนลูกค้าจำนวนมาก',
      dateOpen: '2021-02-02',
      dateClose: '2021-03-02',
      status: 'เปิดรับสมัครอยู่'
    },
    {
      id: 2,
      name: 'รับงานตำแหน่ง programmer',
      career: 'programmer/it',
      position: 'programmer',
      type: 'ประจำ',
      companyAddress: 'ตึกกรุงเทพ ชั้น 5 เลขที่ 179 ถนนสาทรใต้ เขตทุ่งมหาเมฆ',
      cityAddress: 'Chonburi',
      salary: 45000,
      amount: 1,
      benefit: '-ค่าเดินทาง -ค่าที่พัก -ค่าอาหาร -ค่าโทรศัพท์ -ประกันสังคม',
      dateOpen: '2021-02-02',
      dateClose: '2021-03-02',
      status: 'เปิดรับสมัครอยู่'
    }
  ],
  lastId: 3,
  addAnm (anm) {
    anm.id = this.lastId++
    this.AnmsList.push(anm)
  },
  updateAnm (anm) {
    const index = this.AnmsList.findIndex(item => item.id === anm.id)
    this.AnmsList.splice(index, 1, anm)
  },
  deleteAnm (anm) {
    const index = this.AnmsList.findIndex(item => item.id === anm.id)
    this.AnmsList.splice(index, 1)
  },
  getAnms () {
    return [...this.AnmsList]
  }
}
export default anounmentService
